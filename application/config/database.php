<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    // Подключение по умолчанию
    'default' => array
    (
        'type'       => 'mysqli',
        'connection' => array(
            'hostname'   => 'localhost',
            'database'   => 'kohana_medusa',
            'username'   => 'root',
            'password'   => FALSE,
            'persistent' => FALSE,
        ),
        'table_prefix' => '',
        'charset'      => 'utf8',
        'caching'      => FALSE,
    ),
    // Альтернативное подключение
    'alternate' => array(
        'type'       => 'PDO',
        'connection' => array(
            'dsn'        => 'mysql:host=localhost;dbname=kohana_medusa',
            'username'   => 'root',
            'password'   => '',
            'persistent' => FALSE,
        ),
        'table_prefix' => '',
        'charset'      => 'utf8',
        'caching'      => FALSE,
    ),
);