<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Action extends Controller {

    public function action_content()
    {
        if(Auth::instance()->logged_in())
        {
            $news = Model::factory('news');
            $user = Auth::instance()->get_user();
            $this->response->body(json_encode($news->show_news($user)));
        }
    }

    public function action_registration()
    {
        $user = Model::factory('user');

        $validation = Validation::factory($this->request->post());
        $validation
            ->rule('login', 'not_empty')
            ->rule('login', 'regex', array(':value', '/^[a-z_.]++$/iD'))
            ->rule('login', array($user, 'unique_login'))
            ->rule('password', 'not_empty')
            ->rule('password', 'min_length', array(':value', '6'))
            ->rule('email', 'email')
            ->rule('email', 'email_domain')
            ->rule('email', array($user, 'unique_email'));

        if ($validation->check())
        {
            $user->register($this->request->post());
            $this->response->body('ok');
        }
        else
        {
            $this->response->body(json_encode($validation->errors('user')));
        }
    }

    public function action_login()
    {
        if (Auth::instance()->login($this->request->post('login'), $this->request->post('password')))
        {
            $this->response->body('ok');
        }
        else
        {
            $this->response->body('failed');
        }
    }

    public function action_logout()
    {
        Auth::instance()->logout(TRUE, TRUE);
    }

    public function action_forgot()
    {
        $user = Model::factory('user');

        $validation = Validation::factory($this->request->post());
        $validation
            ->rule('password', 'not_empty')
            ->rule('password', 'min_length', array(':value', '6'))
            ->rule('email', 'email')
            ->rule('email', 'email_domain')
            ->rule('email', array($user, 'exist_email'));

        if ($validation->check()) {
            $email = $this->request->post('email');
            $reset_password = Model::factory('resetPassword');
            $token = $reset_password->register($email, $this->request->post('password'));

            if ($token != null)
            {
                $url = URL::base('https', false) . 'ajax/action/new_password/?t=' . $token;
                Email::instance()
                    ->from('sender@medusa.com')
                    ->to($email)
                    ->subject('Restart password')
                    ->message('For reset password, go here: ' . $url)
                    ->send();
            }
        }
    }

    public function action_new_password()
    {
        $token = $this->request->query('t');
        $reset_password = Model::factory('resetPassword');
        $reset_password->update_password($token);
    }

    public function action_exchangerate()
    {
        $currency = Model::factory('currency');
        $this->response->body($currency->value($this->request->post('currency')));
    }

    public function action_word()
    {
        $validation = Validation::factory($this->request->post());
        $validation
            ->rule('word', 'not_empty');

        if ($validation->check()) {
            $word = Model::factory('word');
            $word->register($this->request->post('word'));
            $this->response->body('ok');
        }
    }

    public function action_words()
    {
        $validation = Validation::factory($this->request->post());
        $validation
            ->rule('start', 'numeric')
            ->rule('limit', 'numeric');

        if ($validation->check())
        {
            $word = Model::factory('word');
            $response = $word->search($this->request->post('start'), $this->request->post('limit'));
            $this->response->body(json_encode($response));
        }
    }

    public function action_wordstat()
    {
        $word = Model::factory('word');
        $response = $word->search_stat();
        $this->response->body(json_encode($response));
    }

} // End Action