﻿<?php defined('SYSPATH') or die('No direct script access.');

class Model_News extends ORM {

    protected $_table_name = 'news';

    public function show_news($user)
    {
        $data_arr = $this->parse();
        shuffle($data_arr);

        $old_news = $this->download_news($user->id);
        foreach ($data_arr as $data)
        {
            if (!in_array($data['link'], $old_news))
            {
                $this->add_link($user->id, $data['link']);
                return $data;
            }
        }

        $this->clear_link($user->id);
        $this->add_link($user->id, $data_arr[0]['link']);
        return $data_arr[0];
    }

    private function parse()
    {
        Simplehtmldom::init();
        $xml = simplexml_load_file('https://meduza.io/rss/all');
        $values = [];
        foreach ($xml->channel[0]->item as $i)
        {
            $value = [];
            $value['title'] = (string) $i->title;
            $value['description'] = (string) $i->description;
            $value['link'] = $i->link;
            $value['image'] = strpos($i->enclosure['type'], 'image') !== false ? $i->enclosure['url'] : null;

            array_push($values, $value);
        }

        return $values;
    }

    private function download_news($user_id)
    {
        $response = ORM::factory('news')
            ->where('user_id', '=', $user_id)
            ->find_all();

        $result = [];
        foreach ($response as $value)
        {
            array_push($result, $value->link);
        }

        return $result;
    }

    private function add_link($user_id, $news_link)
    {
        $news = ORM::factory('news');

        $news->user_id = $user_id;
        $news->link = $news_link;

        $news->save();
    }

    private function clear_link($user_id)
    {
        $items = ORM::factory('news')
            ->where('user_id', '=', $user_id)
            ->find_all();

        foreach ($items as $item)
        {
            $item->delete();
        }
    }
}