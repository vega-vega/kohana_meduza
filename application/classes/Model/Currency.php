<?php defined('SYSPATH') or die('No direct script access.');

class Model_Currency extends Model {

    private $valid_char = ['USD', 'EUR', 'GBP'];

    public function value($currency)
    {
        $currency = strtoupper($currency);
        if (in_array($currency, $this->valid_char))
        {
            return $this->parse_xml($currency);
        }

        return null;
    }

    private function parse_xml($char_code)
    {
        $date = date('d/m/Y');
        $xml = simplexml_load_file('http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . $date);
        foreach ($xml->Valute as $Valute)
        {
            if ($Valute->CharCode == $char_code)
            {
                return round(str_replace(',','.',$Valute->Value), 2);
            }
        }
    }
}
