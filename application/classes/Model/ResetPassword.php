<?php defined('SYSPATH') or die('No direct script access.');

class Model_ResetPassword extends Model {

    public function register($email, $password)
    {
        $user = ORM::factory('user')->where('email', '=', $email)->find();
        if ($user->loaded())
        {
            $now = new DateTime();
            $values = [
                'user_id' => $user->id,
                'password' => $password,
                'token' => bin2hex(openssl_random_pseudo_bytes(16)),
                'created' => $now->getTimestamp()
            ];

            DB::insert('reset_password', array_keys($values))
                ->values($values)
                ->execute();

            return $values['token'];
        }
        else
        {
            return null;
        }
    }

    public function update_password($token)
    {
        $query = DB::select('user_id', 'password')
            ->from('reset_password')
            ->where('token', '=', $token)
            ->execute();

        $value = $query->current();
        if ($value != null)
        {
            $user = ORM::factory('user')
                ->where('id', '=', $value['user_id'])
                ->find();

            $user->password = $value['password'];
            $user->update();

            DB::delete('reset_password')
                ->where('user_id', '=', $value['user_id'])
                ->execute();
        }
    }
}