<?php defined('SYSPATH') or die('No direct script access.');

class Model_User extends Model_Auth_User {

    public static function unique_login($login)
    {
        // Check if the login already exists in the database
        return ! DB::select(array(DB::expr('COUNT(login)'), 'total'))
            ->from('users')
            ->where('login', '=', $login)
            ->execute()
            ->get('total');
    }

    public static function unique_email($email)
    {
        // Check if the email already exists in the database
        return ! DB::select(array(DB::expr('COUNT(email)'), 'total'))
            ->from('users')
            ->where('email', '=', $email)
            ->execute()
            ->get('total');
    }

    public static function exist_email($email)
    {
        return ORM::factory("user")->where('email', '=', $email)->find();
    }

    public function get_user_by_email($email)
    {
        $user = ORM::factory("user")->where('email', '=', $email)->find();
        if ($user->loaded())
        {
            return $user;
        }
        else
        {
            return null;
        }
    }

    public function register($array)
    {
        $user = ORM::factory("user");

        $user->login = $array['login'];
        $user->password = $array['password'];
        $user->email = $array['email'];

        $user->save();
        $user->add('roles',ORM::factory('role', array('name' => 'login')));
    }

    public function unique_key($value)
    {
        return Valid::email($value) ? 'email' : 'login';
    }

    public function rules()
    {
        return array(
            'login' => array(
                array('not_empty'),
                array('max_length', array(':value', 32)),
                array(array($this, 'unique'), array('login', ':value')),
            ),
            'password' => array(
                array('not_empty'),
            ),
            'email' => array(
                array('not_empty'),
                array('email'),
                array(array($this, 'unique'), array('email', ':value')),
            ),
        );
    }

    public function labels()
    {
        return array(
            'login'         => 'login',
            'email'            => 'email address',
            'password'         => 'password',
        );
    }
}
