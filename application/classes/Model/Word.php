<?php defined('SYSPATH') or die('No direct script access.');

class Model_Word extends ORM {

    protected $_table_name = 'words';

    public function register($word)
    {
        $model_word = ORM::factory("word");
        $model_word->word = $word;
        $model_word->save();
    }

    public function search($start, $limit)
    {
        $start = null === $start ? 0 : $start;
        $limit = null === $limit ? 3 : $limit;

        $factory =  ORM::factory("word")
            ->limit($limit)
            ->offset($start);

        $response = [];
        foreach($factory->find_all() as $row)
        {
            array_push($response, $row->word);
        }

        return $response;
    }

    public function search_stat()
    {
        return DB::select('word', array(DB::expr("COUNT('word')"), 'cnt'))
            ->from('words')
            ->group_by('word')
            ->order_by('cnt','desc')
            ->execute()
            ->as_array();
    }
}
